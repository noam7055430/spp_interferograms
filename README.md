# Interferograms in Neutron spectroscopy

In this repository, there are some Matlab scripts for calculation an visualization of Neutron Interferograms. 
We simulate experiments. where a spiral phase plate (SPP) 
 is put into a perfect crystal neutron interferometer and we calculate the expected interference pattern on the neutrondetector.

![SPP](figures_nature_paper/html/Fig_7_Spiral_Phase_Plate_01.png)


## Authors of the Matlab-Code
The Matlab-Scripts have been implemented by 
Wolfgang Treimer and Frank Haußer, Berliner Hochschule für Technik;
Wolfgang.Treimer@bht-berlin.de, Frank.Hausser@bht-berlin.de

## Acknowledgment
Martin Suda provided important parts of the mathematical modelling used in the simulations

## License
Creative Commons Attribution Share Alike 4.0 license.

https://creativecommons.org/licenses/by-sa/4.0/

