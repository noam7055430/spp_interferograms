%% interferograms for step sizes L = 1, 2, 3

%--------------------------------------------------------------------------
%
% Authors: Wolfgang Treimer, wolfgang.treimer@bht-berlin.de
%          Frank Haußer, frank.hausser@bht-berlin.de
% Date  :  7 / 2023
%
% License CC BY-SA 4.0 , https://creativecommons.org/licenses/by-sa/4.0/
%
%--------------------------------------------------------------------------

clearvars
close all

%% Material parameter (Spiral phase plate SPP)

lambda = 2.71e-10;          % wavelength neutron [m]

%Silicon data
a = 5.4309962*10^-10;       % lattice constant [m]
rho_Si = 2329.038;          % density [kg/m^3}
A_Si   =  0.028085441;      % atomic weight [kg/mol]
bc_Si  = 4.1491*10^-15;     % coherent scattering length [m]
NA     = 6.02214078*10^23;  % atoms / Mol ( Loschmidt number 
N_Si   = NA*rho_Si/A_Si;    % number of atoms per m^3 
%n_Si   = 1 - lambda^2*N_Si*bc_Si/(2*pi); % refractive index     %      
D_lambda_Si = 2*pi/(lambda*N_Si*bc_Si)*1.e3 % [mm]

%% Simulation parameter
R        = 7.5;         % [mm] Radius of spiral
D_Lambda = D_lambda_Si; % [mm]
L        = 3.;          % Spiral step in units of D_Lambda 
hs_y     = L*D_Lambda;  % [mm]  spiral step size
res      = 0.1;         % [mm] spatial resolution of detector (pixel size)
N        = ceil(R/res); % number of pixels (sensorarray elements) in x and z direction 
                        % (spacial resolution as in [Clark et al])
                        
% discretization of x-z-plane
x = linspace(-R, R, N);
z = linspace(-R, R, N);
[xi, zi] = meshgrid(x, z);

phi_0    = -50*pi/180;
n_avg    = 7;          % mask size [pixels] for averaging the intensity pattern
noise    = true;       % adding multiplikative noise to signal (before averaging)


%% Calculate and plot interference pattern for L= 1, 2, 3; 
figure(1)
plot_nr = 1;
%figure('Color',[1 1 1]);

for L = [1, 3, 2]
    subplot(2, 2, plot_nr)
    di             = spiralthickness(xi, zi, L);
    int_c          = intensity_c(phi_0, di);
    int_c_circ     = circular_mask(xi, zi, int_c, R);
    int_c_avg_circ = noise_and_average(int_c_circ, n_avg, noise);
    titlestring = ['L = ', num2str(L)];
    plot_intensity(xi, zi, int_c_avg_circ, titlestring)
    plot_nr = plot_nr + 1;
    phi_0 = phi_0-3.5 ;
end

annotation('textbox',...
    [0.494 0.4987 0.0311 0.05556],...
    'String',{'+'},...
    'FontSize',18,...
    'FontName','Arial',...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);
annotation('arrow',[0.439869791666667 0.505464409722222],...
    [0.764549768518519 0.533304398148148]);
annotation('arrow',[0.440421006944445 0.506015625],...
    [0.265516203703704 0.499496527777778]);


%% Local functions

function [y] = circular_mask(x, z, y, radius)
    ind    =  x.^2 + z.^2 > radius^2;
    y(ind) = 0;
end

function [int_c] =  intensity_c(phi_0, d)
    int_c = 1./2 + 1/2*cos(phi_0 + 2*pi*d);
end

function [int_c] = noise_and_average(int_c, n_avg, noise)
    if noise == true
        int_c = int_c.*rand(size(int_c));
    end
    detektor_mask = ones(n_avg, n_avg)/n_avg/n_avg;           
    int_c = conv2(int_c, detektor_mask, 'same');
    int_c = int_c/max(int_c(:));
end

function [d] = spiralthickness(x, z, L)
% centre is origin of (x,z)-plane, thickness in y-direction  
    theta_y = atan2(x, z); 
    shift_y = L*theta_y/(2*pi);
    d = L/2 + shift_y;
end

function plot_intensity(xi, zi, int_c, titlestring)
   s = surf(xi, zi, int_c);
   s.EdgeColor = 'None';
   axis([min(xi(:)) max(xi(:)), min(zi(:)), max(zi(:)), 0 1]); 
   xlabel('[mm]')
   ylabel('[mm]')
   title(titlestring, 'FontSize',12,'FontName', 'Book Antiqua')
   view(90,90)
   colormap(jet) 
   axis square
   xticks([-5 0 5]) 
   yticks([-5 0 5]) 
   zticks([ 0 1]) 
end
