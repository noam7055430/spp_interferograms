%% Calculation of Borrmann-Delta

%--------------------------------------------------------------------------
%
% Authors: Wolfgang Treimer, wolfgang.treimer@bht-berlin.de
%          Frank Haußer, frank.hausser@bht-berlin.de
% Date  :  7 / 2023
%
% License CC BY-SA 4.0 , https://creativecommons.org/licenses/by-sa/4.0/
%
%--------------------------------------------------------------------------

clearvars;  
close all;

%% Material parameter

% wavelength neutron
lambda = 2.71e-10;        % [m]

% Silicon crystal data
a_Si = 5.4309962*10^-10;    % lattice constant [m]
rho_Si = 2329.038;          % density [kg/m^3}
A_Si   =  0.028085441;      % atomic weight [kg/mol]
bc_Si  = 4.1491*10^-15;     % coherent scattering length [m]
NA     = 6.02214078*10^23;  % atoms / Mol ( Loschmidt number )
N_Si   = NA*rho_Si/A_Si;    % number of atoms per m^3        
D_lambda_Si = 2*pi/(lambda*N_Si*bc_Si)*1.e3; % [mm]

% Reflexion plane
h1 = 1; h2 = 1; h3 = 1 ;     % Miller- Indices
dhkl       = a_Si/sqrt(h1^2 + h2^2 + h3^2);
thetaB     = asin(lambda/(2*dhkl)) ;  %  Bragg angle 

% Refractive index n 
n  = 1 - (((lambda)^2)*N_Si*bc_Si)/(2*pi);
delta = 1 - n;   

% Form factors of neutrons
DWF = 0.988623;         % Debye-Waller-Faktor
Fu = DWF*sqrt(32);
Fg = DWF*8;

% Interaction Potentials -- Attention:  Fu, Fg !!
V0 = N_Si*bc_Si*lambda^2/pi;
VG = Fu*bc_Si*lambda^2/(pi*a_Si^3) ; % VG gerader Reflex Fg !!

%% Calculation 

y1 = -3:0.001:3;
gAMMA = y1./sqrt(1 + y1.^2 ) ;
D_Kr = 2.0*10^-3; % crystal thickness [m] !!  IFM crystal (Diss)
A   = bc_Si*Fu*lambda/a_Si^3*D_Kr/cos(thetaB)- 0.5; 
T2  = (1-gAMMA.^2);  
PG  = (sin(A*sqrt(T2)+pi/4)).^2./sqrt(T2);

%% visualization
plot(gAMMA, PG,'k')
grid on
xlabel('\Gamma')
ylabel('P_G')
legend('P_G')
annotation('textbox',...
[0.3 0.53 0.39 0.18],...
    'String',{'Crystal thickness = 2mm','\lambda = 0.271 nm','Darwin width = 1.79 arcsec'},...
    'FitBoxToText','on','FitBoxToText','on',...
    'EdgeColor','none');
 grid on
