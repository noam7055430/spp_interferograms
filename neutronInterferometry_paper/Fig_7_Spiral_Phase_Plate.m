%% Visualization of the spiral phase plate

%--------------------------------------------------------------------------
%
% Authors: Wolfgang Treimer, wolfgang.treimer@bht-berlin.de
%          Frank Haußer, frank.hausser@bht-berlin.de
% Date  :  7 / 2023
%
% License CC BY-SA 4.0 , https://creativecommons.org/licenses/by-sa/4.0/
%
%--------------------------------------------------------------------------

clearvars
close all

%% Parameters
R    = 7.5; % Radius of spiral (in units of D_Lambda :  sollte mm sein)
hs_y = 1;   % Spiral step in units of D_Lambda) 
N    = 500; % pixel in x/z direction

%% Calculate thickness function 

x = linspace(-R, R, N);
z = linspace(-R, R, N);

[xi, zi] = meshgrid(x, z);
di       = spiralthickness(xi, zi, hs_y);
di_circ  = circular_mask(xi, zi, di, R);

%% Visualization

figure(1),
mesh(xi, zi, di_circ);
xlabel('mm', 'FontSize', 14, 'FontName', 'Book Antiqua')
ylabel('mm', 'FontSize', 14, 'FontName', 'Book Antiqua')
zlabel('D_\lambda', 'FontSize', 14, 'FontName', 'Book Antiqua')
colormap(jet)
view(-45,60)
grid off
xticks([-5 0 5 ])
yticks([-5 0 5 ])   
zticks([0 1])
xlim([-7.5, 7.5])
ylim([-7.5, 7.5])

figure(2)    
di_circ = imrotate(di_circ,270);
di_circ = flipud(di_circ)  ;
imagesc(x, z, di_circ)
xlabel('mm', 'FontSize', 14, 'FontName', 'Book Antiqua')
ylabel('mm', 'FontSize', 14, 'FontName', 'Book Antiqua')
zlabel('D_\lambda', 'FontSize', 14, 'FontName', 'Book Antiqua')
axis square
colormap(jet)
colorbar('Position', [0.87 0.0996 0.025 0.8156]);
annotation('textbox', [0.902 0.825 0.0544 0.0936],...
    'String','D_{\lambda}', 'FontSize',14,'FontName','Book Antiqua',...
    'FitBoxToText','off','EdgeColor','none');
xticks([-5 0 5 ])
yticks([-5 0 5 ])    
  

%% local functions

function y = circular_mask(x, z, y, radius)
    ind    =  x.^2 + z.^2 > radius^2;
    y(ind) = 0;
end

function [d] = spiralthickness(x, z, hs_y)
% centre is origin of (x,z)-plane, thickness in y-direction  
    theta_y = atan2(x, z); 
    shift_y = hs_y*theta_y/(2*pi);
    d = hs_y/2 + shift_y;
end

